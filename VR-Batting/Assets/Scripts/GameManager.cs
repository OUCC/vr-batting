using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace battingGame
{
    public class GameManager : MonoBehaviour
    {
        //perfectの時間
        float justTime = 0.0f;
        //playerの時間
        private float playerTime = 0.0f;

        //タイミングの判定
        Judges judge;
        //トータルスコア
        int score;

        bool isBatting;
        bool isFirst;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("開始");
        }

        // Update is called once per frame
        void Update()
        {
            if (isFirst)
            {
                MainGame();
            }
        }

        void MainGame()
        {
            isFirst = false;

            for (int i = 1; i <= 10; i++)
            {
                OneGame();
            }
        }
        //一回のゲーム
        void OneGame()
        {
            //投手のモーションを再生

            oneGameTimer();
            Judges judge = TimeJudge(playerTime, justTime);
            switch (judge)
            {
                //perfectの処理
                case Judges.perfect:

                    break;
                //goodの処理
                case Judges.good:

                    break;
                //badの処理
                case Judges.bad:

                    break;
                //missの処理
                case Judges.miss:

                    break;
            }
        }
        private void oneGameTimer()
        {
            playerTime += Time.deltaTime;
            if (isBatting)
            {
                return;
            }
        }
        //バットの衝突を判定
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Bat")
            {
                isBatting = true;
                Debug.Log("playerが打った");
            }
        }


        //タイミングの判定
        private Judges TimeJudge(float playerTime, float justTime)
        {
            float timeGap = justTime - playerTime;
            if (timeGap < 0)
            {
                timeGap = -timeGap;
            }

            if (timeGap <= 4)
            {
                Debug.Log("perfect");
                return Judges.perfect;
            }
            else if (timeGap <= 8)
            {
                Debug.Log("good");
                return Judges.good;
            }
            else if (timeGap <= 12)
            {
                Debug.Log("bad");
                return Judges.bad;
            }
            else
            {
                Debug.Log("miss");
                return Judges.miss;
            }
        }

        enum Judges
        {
            perfect,
            good,
            bad,
            miss
        }
    }
}
